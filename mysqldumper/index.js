/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

var shell = require('shelljs'),
        path = require('path');
var mkdirp = require('mkdirp');
var mysql = require('mysql');
var fs = require('fs');
var crypto = require('crypto');
var nconf = require('nconf');
var readline = require('readline');
var colors = require('colors/safe');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function ms() {

    this.host = "";
    this.port = 3389;
    this.database = "";
    this.user = "";
    this.pass = "";
    this.path = "";
    this.ConfigJSON = "";

}
;

ms.prototype.init = function () {

    var ConfigTitel = "";
    var Host = "";
    var Port = "";
    var User = "";
    var Password = "";
    var NoPass = false;
    var Database = "";
    var ExportPath = "";
    var MysqldumpPfad = "";
    var All = "";
    var Tables = "";
    var Procedure = "";
    var Time = "";
    var Hash = "";


    rl.question(colors.green("Give Your Titel for your Config File") + colors.grey(" (Default: config-mysqldumper.json)") + "\n\n", function (answer) {

        if (answer == "") {

            ConfigTitel = "config-mysqldumper.json";
        } else {

            ConfigTitel = answer;
        }
        rl.question(colors.green("Give Your DB Hostname") + colors.grey(" (Default: localhost)") + "\n\n", function (answer) {

            if (answer == "") {

                Host = "localhost";
            } else {

                Host = answer;
            }
            rl.question(colors.green("Give Your DB Port") + colors.grey(" (Default: 3306)") + "\n\n", function (answer) {

                if (answer == "") {

                    Port = "3306";
                } else {

                    Port = answer;
                }
                rl.question(colors.green("Give Your DB User") + colors.grey(" (Default: root)") + "\n\n", function (answer) {

                    if (answer == "") {

                        User = "root";
                    } else {

                        User = answer;
                    }
                    rl.question(colors.green("Give Your Passwort") + colors.grey(" (Default: No Passwort)") + "\n\n", function (answer) {

                        if (answer == "") {
                            NoPass = false
                            Password = "";
                        } else {

                            Password = answer;
                        }

                        rl.question(colors.green("Give Your Databasename") + colors.grey(" (Default: test)") + "\n\n", function (answer) {

                            if (answer == "") {

                                Database = "test";
                            } else {

                                Database = answer;
                            }
                            rl.question(colors.green("Give Your ExportPath") + colors.grey(" (Default: Database)") + "\n\n", function (answer) {

                                if (answer == "") {

                                    ExportPath = "Database";
                                } else {

                                    ExportPath = answer;
                                }

                                rl.question(colors.green("Give Your the Path from Mysqldumper(exe)") + "\n\n", function (answer) {

                                    if (answer == "") {

                                        MysqldumpPfad = "";
                                    } else {

                                        MysqldumpPfad = answer;
                                    }


                                    rl.question(colors.green("Want you Export All Data in Onefile (true/false)") + colors.grey(" (Default: true)") + "\n\n", function (answer) {

                                        if (answer == "") {

                                            All = true;
                                        } else {

                                            All = answer;
                                        }
                                        rl.question(colors.green("Want you Export Tables (true/false)") + colors.grey(" (Default: false)") + "\n\n", function (answer) {

                                            if (answer == "") {

                                                Tables = false;
                                            } else {

                                                Tables = answer;
                                            }
                                            rl.question(colors.green("Want you Export Procedure (true/false)") + colors.grey(" (Default: false)") + "\n\n", function (answer) {

                                                if (answer == "") {

                                                    Procedure = true;
                                                } else {

                                                    Procedure = answer;
                                                }
                                                rl.question(colors.green("Give the Check Interval for the Watcher") + colors.grey(" (Default: 10000)") + "\n\n", function (answer) {

                                                    if (answer == "") {

                                                        Time = 10000;
                                                    } else {

                                                        Time = answer;
                                                    }

                                                    nconf.use('file', {file: process.cwd() + "\\" + ConfigTitel});
                                                    nconf.set("Host", Host);
                                                    nconf.set("Port", Port);
                                                    nconf.set("User", User);
                                                    nconf.set("Pass", Password);
                                                    nconf.set("NoPass", NoPass);
                                                    nconf.set("Database", Database);
                                                    nconf.set("ExportPath", ExportPath);
                                                    nconf.set("MysqldumpPfad", MysqldumpPfad);
                                                    nconf.set("All", All);
                                                    nconf.set("Tables", Tables);
                                                    nconf.set("Procedure", Procedure);
                                                    nconf.set("Hash", "");
                                                    nconf.set("Time", Time);
                                                    nconf.save();







                                                    rl.close();
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });

                    });
                });
            });
        });
    });





};




ms.prototype.RunWatcher = function () {

    var Config = this.ConfigJSON;
    setInterval(function () {

        var tr = new ms();
        tr.setConfigJSON(Config);
        tr.RunUpdater();


    }, this.time);



};











ms.prototype.RunUpdater = function () {

 try{
   
    var Hash = "asd";
    var Config = this.ConfigJSON;



    nconf.use('file', {file: process.cwd() + "\\" + this.ConfigJSON});
    nconf.load();
    var connection = mysql.createConnection({
        multipleStatements: true,
        host: this.host,
        user: this.user,
        password: this.pass,
        database: this.database,
        port: this.port
    });

    var Database = this.database;
    connection.connect();
    connection.query('select * from information_schema.columns where table_schema = "' + Database + '" ;' + 'SHOW TRIGGERS IN ' + Database, function (err, results) {

        var shasum = crypto.createHash('sha512');
        shasum.update(JSON.stringify(results[0]) + JSON.stringify(results[1]));
        var HashTable = shasum.digest('hex');

        Hash = HashTable;
    
        if (nconf.get('Hash') != Hash) {




            nconf.set('Hash', Hash);
            nconf.save(function (err) {
                if (err) {
                    console.error(err.message);
                    return;
                }


                var tr = new ms();
                tr.setConfigJSON(Config);
                tr.export();

            });

        } else {

            console.log("No Update Need");

        }

    });
    connection.end();

}catch(e){

    console.log("The Configuration in the Config Data has Failure");

}

};




ms.prototype.setConfigJSON = function (ConfigJSON) {

    try{
    var mysqldumper =  process.cwd() + "\\" + ConfigJSON;
    var parsedJSON = require(mysqldumper);

    this.host = parsedJSON.Host;
    this.database = parsedJSON.Database;
    this.port = parsedJSON.Port;
    this.user = parsedJSON.User;
    this.pass = parsedJSON.Pass;
    this.path = parsedJSON.ExportPath;
    this.mysqldumpPfad = parsedJSON.MysqldumpPfad;
    this.time = parsedJSON.Time;
    this.all = parsedJSON.All;
    this.table = parsedJSON.Tables;
    this.procedure = parsedJSON.Procedure;
    this.ConfigJSON = ConfigJSON;
        
        return true;
        
    }catch(e){
    
        console.log("The Config File is not Found");
        
           return false;
        
    }


};

ms.prototype.export = function () {
    
try{

    var connection = mysql.createConnection({
        host: this.host,
        user: this.user,
        password: this.pass,
        database: this.database,
        port: this.port
    });

    var mysqldumper =  process.cwd();


    var cmd_all = "<%= mysqldumper %> --hex-blob -h<%= host %> -P<%= port %> -u<%= user %> -p<%= pass %> <%= database %>";
    cmd_all = cmd_all.replace("<%= host %>", this.host);
    cmd_all = cmd_all.replace("<%= port %>", this.port);
    cmd_all = cmd_all.replace("<%= user %>", this.user);
    cmd_all = cmd_all.replace("<%= pass %>", this.pass);
    cmd_all = cmd_all.replace("<%= database %>", this.database);
    cmd_all = cmd_all.replace("<%= mysqldumper %>", this.mysqldumpPfad);


    var MyPath = this.path;



    /****************************************************************************
     * 
     * 
     *                                   All Data
     * 
     * 
     ***************************************************************************/

    if (this.all == true) {
        mkdirp(MyPath + "/All", function (err) {
        });

        console.log("Step 1: Export All Data in One File .....");

        var ret = shell.exec(cmd_all + " --routines --triggers", {silent: true});

        if (ret.code != 0)
        {
            console.log(ret.output);
            return false;
        }


        fs.writeFile(mysqldumper + "\\" + this.path + "\\All\\all.sql", ret.output.replace("Warning: Using a password on the command line interface can be insecure.",""), function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("Step 1: Export Finish");
            }
        });
    }
    if (this.table == true) {

        connection.connect();


        /****************************************************************************
         * 
         * 
         *                                   Table Export
         * 
         * 
         ***************************************************************************/

        connection.query('SELECT * FROM information_schema.tables WHERE TABLE_SCHEMA ="' + this.database + '"', function (err, rows, fields) {
            if (err)
                throw err;

            mkdirp(MyPath + "/Tables", function (err) {
            });

            console.log("Step 2: Export Table in One File .....");


            for (var i = 0; i < rows.length - 1; i++) {


                mkdirp(MyPath + "/Tables/" + rows[i].TABLE_NAME, function (err) {
                });

                var cmd_table_stuct = cmd_all + " -d " + rows[i].TABLE_NAME;

                var ret_table_struct = shell.exec(cmd_table_stuct, {silent: true});

                if (ret_table_struct.code != 0)
                {
                    console.log(ret_table_struct.output);
                    return false;
                }

                fs.writeFile(mysqldumper + "\\" + MyPath + "\\Tables\\" + rows[i].TABLE_NAME + "\\" + rows[i].TABLE_NAME + "-no-data.sql", ret_table_struct.output.replace("Warning: Using a password on the command line interface can be insecure.",""), function (err) {
                    if (err) {
                        console.log(err);
                    } else {

                    }
                });

                /*********************************************************
                 * 
                 *    Data
                 * 
                 *********************************************************/
                var cmd_table_data = cmd_all + " --no-create-info --skip-triggers " + rows[i].TABLE_NAME;

                var ret_table_data = shell.exec(cmd_table_data, {silent: true});

                if (ret_table_data.code != 0)
                {
                    console.log(ret_table_data.output);
                    return false;
                }

                fs.writeFile(mysqldumper + "\\" + MyPath + "\\Tables\\" + rows[i].TABLE_NAME + "\\" + rows[i].TABLE_NAME + "-data.sql", ret_table_data.output.replace("Warning: Using a password on the command line interface can be insecure.",""), function (err) {
                    if (err) {
                        console.log(err);
                    } else {

                    }
                });



            }


            console.log("Step 2: Export Finish");

        });

        connection.end();
    }



    if (this.procedure == true) {
        mkdirp(MyPath + "/Procedure", function (err) {
        });

        console.log("Step 3: Export Procudure .....");

        var ret = shell.exec(cmd_all + " --routines --no-create-info --no-data --no-create-db --skip-opt  --skip-triggers", {silent: true});

        if (ret.code != 0)
        {
            console.log(ret.output);
            return false;
        }


        fs.writeFile(mysqldumper + "\\" + this.path + "\\Procedure\\Procedure.sql", ret.output, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("Step 3: Export Finish");
            }
        });

    }

}catch(e){

console.log("The Configuration in the Config Data has Failure");


}


};


module.exports = new ms();


