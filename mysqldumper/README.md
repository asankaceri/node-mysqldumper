
Mysql Dumper 
=======

The Mysql Dumper will help for better Versioniering and Backup the Mysql Database.
This Tool has Diffrent Modi:

 1. Watch the Mysql Database every  x Seconds for New Updates and Export The Database in a Destination Folder.
 2. Check has the Database a new Schema and Export The Database in a Destination Folder.
 3. Export The Database in Folder

You can set in Config File:

> Host :           Example Hist or IP
> Port:             Exampel 3306
> Pass:                     Pass From your Database
> NoPass:                Have you no Pass
> Database:             Database Name
> ExportPath:          Export Path
> MysqldumpPfad:  MysqlDump.exe
> All:                        true / false  => Want you Download all Data + Schema + Procedure +....  in one File
> Tables:                  true / false  => Want you Download every Table in Seperate File
> Procedure:            true / false  => Want you Download  Procedure in Seperate File
> Time:                    Example 1000 => >you can set the Time Intervall, how you want check the Database

## How I can use the Tool ##

First you must Install the Tool over npm global

    npm install mysqldumper -g
   
 Now go to the Project Path and create a config File over(Please follow the wizzard):

    mysqldumper -i
 
 Now you can use for watch the Mysql Database :

     mysqldumper -w

 Now you can use for Check Database and export the Mysql Database :

     mysqldumper -u
     
 Now you can use for Export the Mysql Database :

     mysqldumper -e